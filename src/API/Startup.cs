﻿using DataContext.Infrastructure;
using GraphQL;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using GraphModel.Schema;
using GraphQL.Server;
using GraphModel.Types;

namespace API
{
  public class Startup
  {
    // This method gets called by the runtime. Use this method to add services to the container.
    // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
    public void ConfigureServices(IServiceCollection services)
    {
      services.AddSingleton<IApplicationDbContext, ApplicationDbContext>();
      services.AddSingleton<IDependencyResolver>(x => new FuncDependencyResolver(x.GetRequiredService));

      //##########################################################
      //Companies
      services.AddSingleton<CompanyType>();
      //Persons
      services.AddSingleton<GenderEnumType>();
      //Education
      services.AddSingleton<EducationType>();
      //Project
      services.AddSingleton<ProjectType>();
      //Employment
      services.AddSingleton<EmploymentType>();
      //Skill
      services.AddSingleton<SkillType>();
      //Persons
      services.AddSingleton<PersonsQuery>();
      services.AddSingleton<PersonsSchema>();
      services.AddSingleton<PersonsMutation>();

      services.AddSingleton<PersonType>();
      services.AddSingleton<PersonCreateInputType>();
      services.AddSingleton<PersonUpdateInputType>();



      // Add GraphQL services and configure options
      services.AddGraphQL(options =>
        {
          options.EnableMetrics = true;
          options.ExposeExceptions = true;
        })
        .AddDataLoader(); // Add required services for DataLoader support
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }
      app.UseGraphiQl();
      app.UseDefaultFiles();
      app.UseStaticFiles();
      app.UseWebSockets();
      app.UseGraphQL<PersonsSchema>();


    }
  }
}
