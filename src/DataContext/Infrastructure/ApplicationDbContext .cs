﻿using DataContext.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;

namespace DataContext.Infrastructure
{
  public class ApplicationDbContext : DbContext, IApplicationDbContext
  {
    public DbSet<CompanyEntity> Companies { get; set; }
    public DbSet<EducationEntity> Educations { get; set; }
    public DbSet<EmploymentEntity> Employments { get; set; }
    public DbSet<PersonEntity> Persons { get; set; }
    public DbSet<SkillEntity> Skills { get; set; }
    public DbSet<ProjectEntity> Projects { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
      optionsBuilder.UseSqlServer(@"Server=tcp:127.0.0.1,1433;Initial Catalog=graphQL;Persist Security Info=False;User ID=sa;Password=pokersjov1;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=True;Connection Timeout=30;MultipleActiveResultSets=true;");
    }
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      base.OnModelCreating(modelBuilder);
      modelBuilder.ApplyConfiguration(new CompanyEntityConfiguration());
      modelBuilder.ApplyConfiguration(new EducationEntityConfiguration());
      modelBuilder.ApplyConfiguration(new PersonEntityConfiguration());
      modelBuilder.ApplyConfiguration(new SkillEntityConfiguration());
      modelBuilder.ApplyConfiguration(new ProjectEntityConfiguration());
      modelBuilder.ApplyConfiguration(new EmploymentEntityConfiguration());
    }
    public static ApplicationDbContext Create()
    {
      return new ApplicationDbContext();
    }


  }

  public interface IApplicationDbContext
  {
    DbSet<CompanyEntity> Companies { get; set; }
    DbSet<EmploymentEntity> Employments { get; set; }
    DbSet<PersonEntity> Persons { get; set; }
    DbSet<SkillEntity> Skills { get; set; }
    DbSet<ProjectEntity> Projects { get; set; }
    DbSet<EducationEntity> Educations { get; set; }
    int SaveChanges();
  }
}
