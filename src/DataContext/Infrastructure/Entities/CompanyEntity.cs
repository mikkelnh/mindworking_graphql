﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataContext.Infrastructure.Entities
{
  public class CompanyEntity
  {
    [Key]
    public virtual int Id { get; set; }
    public virtual string Name { get; set; }
    public virtual string Address { get; set; }
    public virtual int? PostalCode { get; set; }
    public virtual string City { get; set; }
    public virtual int? Phone { get; set; }
    public virtual int CVR { get; set; }
    public virtual DateTime Created { get; set; }
  }
  public class CompanyEntityConfiguration : IEntityTypeConfiguration<CompanyEntity>
  {

    public void Configure(EntityTypeBuilder<CompanyEntity> builder)
    {
      builder.HasKey(p => p.Id);
      builder.Property(p => p.Id).ValueGeneratedOnAdd();
      builder.Property(p => p.Name).IsRequired();
      builder.Property(p => p.Address);
      builder.Property(p => p.PostalCode);
      builder.Property(p => p.City);
      builder.Property(p => p.Phone);
      builder.Property(p => p.CVR).HasMaxLength(8);
      builder.Property(p => p.Created).IsRequired();

      builder.ToTable("Company");

      builder.HasData(
        new CompanyEntity() { Id = 1, Name = "PROOFFICE GRUPPEN ApS", CVR = 25770579, Address = "Hasselager Centervej 23", PostalCode = 8260, City = "Viby J", Created = DateTime.UtcNow },
        new CompanyEntity() { Id = 2, Name = "Numea Ivs", CVR = 38636529, Address = "Nordlandsvej 60 F, 1. 4", PostalCode = 8240, City = "Risskov", Created = DateTime.UtcNow },
        new CompanyEntity() { Id = 3, Name = "Office Til Alle ApS", CVR = 36686928, Address = "Ellemosen 3", PostalCode = 8680, City = "Ry", Created = DateTime.UtcNow },
        new CompanyEntity() { Id = 4, Name = "nHorn", CVR = 39270129, Address = "Peter Fabers Vej 3, 1. th", PostalCode = 8210, City = "Aarhus V", Created = DateTime.UtcNow, Phone = 26848120 },
        new CompanyEntity() { Id = 5, Name = "Baader Logistix A/S", CVR = 33786646, Address = "Sindalsvej 44", PostalCode = 8240, City = "Risskov", Created = DateTime.UtcNow },
        new CompanyEntity() { Id = 6, Name = "Mindworking A/S", CVR = 27489974, Address = "Værkmestergade 11, 2", PostalCode = 8000, City = "Aarhus C", Created = DateTime.UtcNow }
        );
    }
  }
}
