﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataContext.Infrastructure.Entities
{
  public class EducationEntity
  {
    [Key]
    public virtual int Id { get; set; }
    public virtual string Name { get; set; }
    [ForeignKey("Student")]
    public virtual int? StudentId { get; set; }
    public virtual PersonEntity Student { get; set; }
  }
  public class EducationEntityConfiguration : IEntityTypeConfiguration<EducationEntity>
  {

    public void Configure(EntityTypeBuilder<EducationEntity> builder)
    {
      builder.HasKey(p => p.Id);
      builder.Property(p => p.Id).ValueGeneratedOnAdd();
      builder.Property(p => p.Name).IsRequired();
      builder.ToTable("Education");
      builder.HasData(
        new EducationEntity() { Id = 1, Name = "Datatechnician speciality infrastructure", StudentId = 1},
        new EducationEntity() { Id = 2, Name = "WebIntegrator", StudentId = 1}
      );
    }
  }
}
