﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataContext.Infrastructure.Entities
{
  public class EmploymentEntity
  {
    [Key]
    public virtual int Id { get; set; }
    [Column(TypeName = "DATETIME2")]
    public virtual DateTime Start { get; set; }
    [Column(TypeName = "DATETIME2")]
    public virtual DateTime? End { get; set; }
    [ForeignKey("Employer")]
    public virtual int EmployerId { get; set; }
    public virtual CompanyEntity Employer { get; set; }
    [ForeignKey("Employee")]
    public virtual int EmployeeId { get; set; }
    public virtual PersonEntity Employee { get; set; }
  }
  public class EmploymentEntityConfiguration : IEntityTypeConfiguration<EmploymentEntity>
  {

    public void Configure(EntityTypeBuilder<EmploymentEntity> builder)
    {
      builder.HasKey(p => p.Id);
      builder.Property(p => p.Id).ValueGeneratedOnAdd();
      builder.Property(p => p.Start).IsRequired();
      builder.Property(p => p.End);
      builder.HasOne(x => x.Employer).WithMany().OnDelete(DeleteBehavior.Cascade);
      builder.HasOne(x => x.Employee).WithMany(p => p.PreviousEmployments).OnDelete(DeleteBehavior.Cascade);

      builder.ToTable("Employment");
      builder.HasData(new EmploymentEntity()
      {
        Id = 1,
        EmployeeId = 1,
        EmployerId = 1,
        Start = new DateTime(2008, 1, 1),
        End = new DateTime(2015, 8, 30)
      }, new EmploymentEntity()
      {
        Id = 2,
        EmployeeId = 1,
        EmployerId = 2,
        Start = new DateTime(2017, 5, 1),
        End = new DateTime(2017, 12, 31)
      }, new EmploymentEntity()
      {
        Id = 3,
        EmployeeId = 1,
        EmployerId = 3,
        Start = new DateTime(2017, 10, 1),
        End = new DateTime(2017, 12, 31)
      }, new EmploymentEntity()
      {
        Id = 4,
        EmployeeId = 1,
        EmployerId = 4,
        Start = new DateTime(2018, 2, 1),
      }, new EmploymentEntity()
      {
        Id = 5,
        EmployeeId = 1,
        EmployerId = 5,
        Start = new DateTime(2017, 8, 1),
      }, new EmploymentEntity()
      {
        Id = 6,
        EmployeeId = 1,
        EmployerId = 6,
        Start = new DateTime(2018, 12, 1)
      });
    }
}
}
