﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DataContext.Infrastructure.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataContext.Infrastructure.Entities
{
  public class PersonEntity
  {
    [Key]
    public virtual int Id { get; set; }
    public virtual string FirstName { get; set; }
    public virtual string LastName { get; set; }
    public virtual int? Age { get; set; }
    public virtual GenderEnum? Gender { get; set; }
    public virtual ICollection<ProjectEntity> Projects { get; set; }
    public virtual ICollection<EmploymentEntity> PreviousEmployments { get; set; }
    public virtual ICollection<EducationEntity> Educations { get; set; }
    public virtual ICollection<SkillEntity> Skills { get; set; }
  }
  public class PersonEntityConfiguration : IEntityTypeConfiguration<PersonEntity>
  {

    public void Configure(EntityTypeBuilder<PersonEntity> builder)
    {
      builder.HasKey(p => p.Id);
      builder.Property(p => p.Id).ValueGeneratedOnAdd();
      builder.Property(p => p.FirstName).IsRequired();
      builder.Property(p => p.LastName).IsRequired();
      builder.Property(p => p.Age);
      builder.Property(p => p.Gender);
      builder.HasMany(x => x.Projects).WithOne(x => x.Owner).OnDelete(DeleteBehavior.Cascade);
      builder.HasMany(x => x.PreviousEmployments).WithOne(x => x.Employee).OnDelete(DeleteBehavior.Cascade);
      builder.HasMany(x => x.Skills).WithOne(x => x.Owner).OnDelete(DeleteBehavior.Cascade);
      builder.HasMany(x => x.Educations).WithOne(x => x.Student).OnDelete(DeleteBehavior.Cascade);

      builder.ToTable("Person");
      builder.HasData(new PersonEntity()
      {
        Id = 1,
        FirstName = "Mikkel",
        LastName = "Horn",
        Age = 29,
        Gender = GenderEnum.Male
      });
    }
  }
}
