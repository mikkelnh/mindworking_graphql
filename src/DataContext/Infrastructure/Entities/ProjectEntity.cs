﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataContext.Infrastructure.Entities
{
  public class ProjectEntity
  {
    [Key] public virtual int Id { get; set; }
    public virtual string Name { get; set; }
    public virtual string Description { get; set; }
    [ForeignKey("Owner")] public virtual int OwnerId { get; set; }
    public virtual PersonEntity Owner { get; set; }
  }

  public class ProjectEntityConfiguration : IEntityTypeConfiguration<ProjectEntity>
  {

    public void Configure(EntityTypeBuilder<ProjectEntity> builder)
    {
      builder.HasKey(p => p.Id);
      builder.Property(p => p.Id).ValueGeneratedOnAdd();
      builder.Property(p => p.Name).IsRequired();
      builder.Property(p => p.Description);
      builder.HasOne(x => x.Owner).WithMany(x => x.Projects).OnDelete(DeleteBehavior.Cascade);
      builder.ToTable("Project");

      builder.HasData(
        new ProjectEntity()
        {
          Id = 1,
          Name = "Samrito",
          Description = "A service to compare prices on online foodmarkets",
          OwnerId = 1
        },
        new ProjectEntity() {Id = 2, Name = "Wideo", Description = "a online e-learning portal", OwnerId = 1},
        new ProjectEntity() { Id = 3, Name = "Valify", Description = "system for detecting fake clothes", OwnerId = 1 },
        new ProjectEntity() { Id = 4, Name = "GraphQL API", Description = "for Mindworking A/S", OwnerId = 1 }
      );
    }
  }
}
