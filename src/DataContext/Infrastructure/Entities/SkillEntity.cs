﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataContext.Infrastructure.Entities
{
  public class SkillEntity
  {
    [Key]
    public virtual int Id { get; set; }
    public virtual string Name { get; set; }
    public virtual string Description { get; set; }
    [ForeignKey("Owner")]
    public virtual int? OwnerId { get; set; }
    public PersonEntity Owner { get; set; }
  }
  public class SkillEntityConfiguration : IEntityTypeConfiguration<SkillEntity>
  {

    public void Configure(EntityTypeBuilder<SkillEntity> builder)
    {
      builder.HasKey(p => p.Id);
      builder.Property(p => p.Id).ValueGeneratedOnAdd();
      builder.Property(p => p.Name).IsRequired();
      builder.Property(p => p.Description);
      builder.ToTable("Skill");


      builder.HasData(
        new SkillEntity() { Id = 1, Name = "T-SQL", Description = "", OwnerId = 1 },
        new SkillEntity() { Id = 2, Name = "Visual Basic", Description = "", OwnerId = 1 },
        new SkillEntity() { Id = 3, Name = "JavaScript", Description = "", OwnerId = 1 },
        new SkillEntity() { Id = 4, Name = "C#", Description = "", OwnerId = 1 },
        new SkillEntity() { Id = 5, Name = "PHP", Description = "", OwnerId = 1 },
        new SkillEntity() { Id = 6, Name = "Python", Description = "", OwnerId = 1 },
        new SkillEntity() { Id = 7, Name = "Java", Description = "", OwnerId = 1 },
        new SkillEntity() { Id = 8, Name = "Assembly", Description = "", OwnerId = 1 },
        new SkillEntity() { Id = 9, Name = "EcmaScript", Description = "", OwnerId = 1 },
        new SkillEntity() { Id = 10, Name = "ActionScript", Description = "", OwnerId = 1 },
        new SkillEntity() { Id = 11, Name = "PowerShell", Description = "", OwnerId = 1 },
        new SkillEntity() { Id = 12, Name = "PhotoShop", Description = "", OwnerId = 1 },
        new SkillEntity() { Id = 13, Name = ".Net", Description = "", OwnerId = 1 },
        new SkillEntity() { Id = 14, Name = "CCNA", Description = "", OwnerId = 1 },
        new SkillEntity() { Id = 15, Name = "CCNI", Description = "", OwnerId = 1 },
        new SkillEntity() { Id = 16, Name = "CCNP", Description = "", OwnerId = 1 },
        new SkillEntity() { Id = 17, Name = "Git Flow", Description = "", OwnerId = 1 },
        new SkillEntity() { Id = 18, Name = "XML", Description = "", OwnerId = 1 },
        new SkillEntity() { Id = 19, Name = "JSON", Description = "", OwnerId = 1 },
        new SkillEntity() { Id = 20, Name = "React", Description = "", OwnerId = 1 },
        new SkillEntity() { Id = 21, Name = "CSS", Description = "", OwnerId = 1 }
      );
    }
  }
}
