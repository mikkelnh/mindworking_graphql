﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataContext.Infrastructure.Enums
{
    public enum GenderEnum
    {
      NotSpecified = 0,
      Male = 1,
      Female = 2
    }
}
