﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataContext.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Company",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Address = table.Column<string>(nullable: true),
                    PostalCode = table.Column<int>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    Phone = table.Column<int>(nullable: true),
                    CVR = table.Column<int>(maxLength: 8, nullable: false),
                    Created = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Company", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Person",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FirstName = table.Column<string>(nullable: false),
                    LastName = table.Column<string>(nullable: false),
                    Age = table.Column<int>(nullable: true),
                    Gender = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Person", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Education",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    StudentId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Education", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Education_Person_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Person",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Employment",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Start = table.Column<DateTime>(type: "DATETIME2", nullable: false),
                    End = table.Column<DateTime>(type: "DATETIME2", nullable: true),
                    EmployerId = table.Column<int>(nullable: false),
                    EmployeeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Employment_Person_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "Person",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Employment_Company_EmployerId",
                        column: x => x.EmployerId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Project",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    OwnerId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Project", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Project_Person_OwnerId",
                        column: x => x.OwnerId,
                        principalTable: "Person",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Skill",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    OwnerId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Skill", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Skill_Person_OwnerId",
                        column: x => x.OwnerId,
                        principalTable: "Person",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Company",
                columns: new[] { "Id", "Address", "CVR", "City", "Created", "Name", "Phone", "PostalCode" },
                values: new object[,]
                {
                    { 1, "Hasselager Centervej 23", 25770579, "Viby J", new DateTime(2018, 9, 4, 0, 56, 13, 662, DateTimeKind.Utc), "PROOFFICE GRUPPEN ApS", null, 8260 },
                    { 2, "Nordlandsvej 60 F, 1. 4", 38636529, "Risskov", new DateTime(2018, 9, 4, 0, 56, 13, 662, DateTimeKind.Utc), "Numea Ivs", null, 8240 },
                    { 3, "Ellemosen 3", 36686928, "Ry", new DateTime(2018, 9, 4, 0, 56, 13, 662, DateTimeKind.Utc), "Office Til Alle ApS", null, 8680 },
                    { 4, "Peter Fabers Vej 3, 1. th", 39270129, "Aarhus V", new DateTime(2018, 9, 4, 0, 56, 13, 662, DateTimeKind.Utc), "nHorn", 26848120, 8210 },
                    { 5, "Sindalsvej 44", 33786646, "Risskov", new DateTime(2018, 9, 4, 0, 56, 13, 663, DateTimeKind.Utc), "Baader Logistix A/S", null, 8240 },
                    { 6, "Værkmestergade 11, 2", 27489974, "Aarhus C", new DateTime(2018, 9, 4, 0, 56, 13, 663, DateTimeKind.Utc), "Mindworking A/S", null, 8000 }
                });

            migrationBuilder.InsertData(
                table: "Person",
                columns: new[] { "Id", "Age", "FirstName", "Gender", "LastName" },
                values: new object[] { 1, 29, "Mikkel", 1, "Horn" });

            migrationBuilder.InsertData(
                table: "Education",
                columns: new[] { "Id", "Name", "StudentId" },
                values: new object[,]
                {
                    { 1, "Datatechnician speciality infrastructure", 1 },
                    { 2, "WebIntegrator", 1 }
                });

            migrationBuilder.InsertData(
                table: "Employment",
                columns: new[] { "Id", "EmployeeId", "EmployerId", "End", "Start" },
                values: new object[,]
                {
                    { 1, 1, 1, new DateTime(2015, 8, 30, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2008, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, 1, 2, new DateTime(2017, 12, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2017, 5, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, 1, 3, new DateTime(2017, 12, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2017, 10, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 4, 1, 4, null, new DateTime(2018, 2, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 5, 1, 5, null, new DateTime(2017, 8, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 6, 1, 6, null, new DateTime(2018, 12, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) }
                });

            migrationBuilder.InsertData(
                table: "Project",
                columns: new[] { "Id", "Description", "Name", "OwnerId" },
                values: new object[,]
                {
                    { 4, "for Mindworking A/S", "GraphQL API", 1 },
                    { 3, "system for detecting fake clothes", "Valify", 1 },
                    { 1, "A service to compare prices on online foodmarkets", "Samrito", 1 },
                    { 2, "a online e-learning portal", "Wideo", 1 }
                });

            migrationBuilder.InsertData(
                table: "Skill",
                columns: new[] { "Id", "Description", "Name", "OwnerId" },
                values: new object[,]
                {
                    { 12, "", "PhotoShop", 1 },
                    { 19, "", "JSON", 1 },
                    { 18, "", "XML", 1 },
                    { 17, "", "Git Flow", 1 },
                    { 16, "", "CCNP", 1 },
                    { 15, "", "CCNI", 1 },
                    { 14, "", "CCNA", 1 },
                    { 13, "", ".Net", 1 },
                    { 11, "", "PowerShell", 1 },
                    { 5, "", "PHP", 1 },
                    { 9, "", "EcmaScript", 1 },
                    { 8, "", "Assembly", 1 },
                    { 7, "", "Java", 1 },
                    { 6, "", "Python", 1 },
                    { 20, "", "React", 1 },
                    { 4, "", "C#", 1 },
                    { 3, "", "JavaScript", 1 },
                    { 2, "", "Visual Basic", 1 },
                    { 1, "", "T-SQL", 1 },
                    { 10, "", "ActionScript", 1 },
                    { 21, "", "CSS", 1 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Education_StudentId",
                table: "Education",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_Employment_EmployeeId",
                table: "Employment",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_Employment_EmployerId",
                table: "Employment",
                column: "EmployerId");

            migrationBuilder.CreateIndex(
                name: "IX_Project_OwnerId",
                table: "Project",
                column: "OwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_Skill_OwnerId",
                table: "Skill",
                column: "OwnerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Education");

            migrationBuilder.DropTable(
                name: "Employment");

            migrationBuilder.DropTable(
                name: "Project");

            migrationBuilder.DropTable(
                name: "Skill");

            migrationBuilder.DropTable(
                name: "Company");

            migrationBuilder.DropTable(
                name: "Person");
        }
    }
}
