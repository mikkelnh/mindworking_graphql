﻿using DataContext.Infrastructure.Enums;

namespace GraphModel.Models
{
  public class PersonCreateInput
  {
    public virtual string FirstName { get; set; }
    public virtual string LastName { get; set; }
    public virtual int? Age { get; set; }
    public virtual GenderEnum? Gender { get; set; }
  }
}
