﻿using GraphQL;
using GraphQL.Types;
using System;
using System.Linq;
using DataContext.Infrastructure;
using DataContext.Infrastructure.Entities;
using GraphModel.Models;
using GraphModel.Types;
using Util;

namespace GraphModel.Schema
{
  public class PersonsSchema : GraphQL.Types.Schema
  {
    public PersonsSchema(PersonsQuery query, PersonsMutation mutation, IDependencyResolver resolver)
    {
      Query = query;
      Mutation = mutation;
      DependencyResolver = resolver;
    }
  }
  public class PersonsQuery : ObjectGraphType<object>
  {
    public PersonsQuery(IApplicationDbContext dbcontext)
    {
      Name = "Query";
      Field<ListGraphType<PersonType>>("persons", resolve: context => dbcontext.Persons);
    }
  }
  public class PersonsMutation : ObjectGraphType<object>
  {
    public PersonsMutation(IApplicationDbContext dbcontext)
    {
      Name = "Mutation";
      Field<PersonType>("createPerson", arguments: new QueryArguments(
        new QueryArgument<NonNullGraphType<PersonCreateInputType>>
        {
          Name = "personCreate"
        }), resolve: context =>
      {
        var input = context.GetArgument<PersonCreateInput>("personCreate");
        var person = new PersonEntity().GatherFrom(input);
        dbcontext.Persons.Add(person);
        dbcontext.SaveChanges();
        return person;
      });

      Field<PersonType>("updatePerson", arguments: new QueryArguments(
        new QueryArgument<NonNullGraphType<PersonUpdateInputType>>
        {
          Name = "personUpdate"
        }), resolve: context =>
      {
        var input = context.GetArgument<PersonUpdateInput>("personUpdate");

        var person = dbcontext.Persons.SingleOrDefault(x => Equals(x.Id, input.Id));
        if (person == null)
        {
          throw new ArgumentException($"Order ID: {input.Id} is invalid");
        }
        person.GatherFrom(input);
        dbcontext.SaveChanges();
        return person;
      });

      Field<IntGraphType>("deletePerson",
        arguments: new QueryArguments(new QueryArgument<NonNullGraphType<IntGraphType>> { Name = "id" }), resolve: 
          context =>
        {
          var personId = context.GetArgument<int>("id");
          var person = dbcontext.Persons.SingleOrDefault(x => Equals(x.Id, personId));
          if (person == null)
          {
            throw new ArgumentException($"Order ID: {personId} is invalid");
          }

          dbcontext.Persons.Remove(person);
          return dbcontext.SaveChanges();
        });
    }
  }
}
