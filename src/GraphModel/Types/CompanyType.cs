﻿using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Text;
using DataContext.Infrastructure;
using DataContext.Infrastructure.Entities;
using GraphModel.Models;

namespace GraphModel.Types
{
  public class CompanyType : ObjectGraphType<CompanyEntity>
  {
    public CompanyType(IApplicationDbContext dbContext)
    {
      Field(x => x.Id);
      Field(x => x.Name);
      Field(x => x.Address);
      Field(x => x.City);
      Field(x => x.PostalCode, nullable: true);
      Field(x => x.CVR);
      Field(x => x.Phone, nullable: true);
      Field(x => x.Created);
    }
  }
  
}
