﻿using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataContext.Infrastructure;
using DataContext.Infrastructure.Entities;
using GraphModel.Models;

namespace GraphModel.Types
{
  public class EducationType : ObjectGraphType<EducationEntity>
  {
    public EducationType(IApplicationDbContext dbContext)
    {
      Field(x => x.Id);
      Field(x => x.Name);
    }
  }
}
