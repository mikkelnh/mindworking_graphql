﻿using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Text;
using DataContext.Infrastructure;
using DataContext.Infrastructure.Entities;
using GraphModel.Models;
using System.Linq;

namespace GraphModel.Types
{
  public class EmploymentType : ObjectGraphType<EmploymentEntity>
  {
    public EmploymentType(IApplicationDbContext dbContext)
    {
      Field(x => x.Id);
      Field(x => x.Start);
      Field(x => x.End, nullable: true);
      Field<ListGraphType<CompanyType>>("Employer", resolve: ctx => dbContext.Companies.Where(x => Equals(ctx.Source.EmployerId, x.Id)));
    }
  }
  
}
