﻿using GraphQL.Types;

namespace GraphModel.Types
{
  public class GenderEnumType : EnumerationGraphType
  {
    public GenderEnumType()
    {
      Name = "GenderEnums";
      AddValue("NotSpecified", "Gender not specified", 0);
      AddValue("Male", "", 1);
      AddValue("Female", "", 2);
    }
  }
}
