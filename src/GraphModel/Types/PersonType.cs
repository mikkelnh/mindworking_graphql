﻿using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataContext.Infrastructure;
using DataContext.Infrastructure.Entities;
using DataContext.Infrastructure.Enums;
using GraphModel.Models;

namespace GraphModel.Types
{
  public class PersonType : ObjectGraphType<PersonEntity>
  {
    public PersonType(IApplicationDbContext dbContext)
    {
      Field(x => x.Id);
      Field(x => x.FirstName);
      Field(x => x.LastName);
      Field(x => x.Age, nullable: true);
      Field<GenderEnumType>("gender", resolve: ctx => ctx.Source.Gender);
      Field<ListGraphType<ProjectType>>("Projects", resolve: ctx => dbContext.Projects.Where(x => Equals(ctx.Source.Id, x.OwnerId)));
      Field<ListGraphType<EmploymentType>>("PreviousEmployments", resolve: ctx => dbContext.Employments.Where(x => Equals(ctx.Source.Id, x.EmployeeId)));
      Field<ListGraphType<EducationType>>("Educations", resolve: ctx => dbContext.Educations.Where(x => Equals(ctx.Source.Id, x.StudentId)));
      Field<ListGraphType<SkillType>>("Skills", resolve: ctx => dbContext.Skills.Where(x => Equals(ctx.Source.Id, x.OwnerId)));

    }
  }

  public class PersonCreateInputType : InputObjectGraphType
  {
    public PersonCreateInputType()
    {
      Name = "PersonCreate";
      Field<NonNullGraphType<StringGraphType>>("firstName");
      Field<NonNullGraphType<StringGraphType>>("lastName");
      Field<StringGraphType>("age");
      Field<NonNullGraphType<GenderEnumType>>("gender");
    }
  }
  public class PersonUpdateInputType : InputObjectGraphType
  {
    public PersonUpdateInputType()
    {
      Name = "PersonUpdate";
      Field<NonNullGraphType<IntGraphType>>("id");
      Field<NonNullGraphType<StringGraphType>>("firstName");
      Field<NonNullGraphType<StringGraphType>>("lastName");
      Field<StringGraphType>("age");
      Field<NonNullGraphType<GenderEnumType>>("gender");
    }
  }
}
