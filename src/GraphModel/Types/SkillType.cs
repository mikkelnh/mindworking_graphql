﻿using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataContext.Infrastructure;
using DataContext.Infrastructure.Entities;
using DataContext.Infrastructure.Enums;
using GraphModel.Models;

namespace GraphModel.Types
{
  public class SkillType : ObjectGraphType<SkillEntity>
  {
    public SkillType(IApplicationDbContext dbContext)
    {
      Field(x => x.Id);
      Field(x => x.Name);
      Field(x => x.Description);
    }
  }
}
