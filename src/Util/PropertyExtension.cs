﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace Util
{
  public static class PropertyExtension
  {
    #region Methods
    public static string GetPropertyName<TProperty>(this Expression<Func<TProperty>> propertyExpression)
    {
      return propertyExpression.Body.GetMemberExpression().GetPropertyName();
    }

    public static string GetPropertyName(this MemberExpression memberExpression)
    {
      if (memberExpression == null)
      {
        return null;
      }

      if (memberExpression.Member.MemberType != MemberTypes.Property)
      {
        return null;
      }

      var child = memberExpression.Member.Name;
      var parent = GetPropertyName(memberExpression.Expression.GetMemberExpression());
      if (parent == null)
      {
        return child;
      }

      return parent + "." + child;
    }

    public static MemberExpression GetMemberExpression(this Expression expression)
    {
      var memberExpression = expression as MemberExpression;
      if (memberExpression != null)
      {
        return memberExpression;
      }

      var unaryExpression = expression as UnaryExpression;
      if (unaryExpression != null)
      {
        memberExpression = (MemberExpression)unaryExpression.Operand;
        if (memberExpression != null)
        {
          return memberExpression;
        }
      }

      return null;
    }

    public static void ShouldEqual<T>(this T actual, T expected, string name)
    {
      if (!Equals(actual, expected))
      {
        throw new Exception(string.Format("{0}: Expected <{1}> Actual <{2}>.", name, expected, actual));
      }
    }

    public static bool HasProperty(this object source, string propertyName)
    {
      return GetTargetProperty(source, propertyName).Property != null;
    }

    public static object GetPropertyValue(this object source, string propertyName)
    {
      var targetProperty = GetTargetProperty(source, propertyName);
      if (targetProperty.IsValid)
      {
        return targetProperty.Property.GetValue(targetProperty.Target, null);
      }

      return null;
    }

    public static void SetPropertyValue(this object source, string propertyName, object value)
    {
      var targetProperty = GetTargetProperty(source, propertyName);
      if (targetProperty.IsValid)
      {
        targetProperty.Property.SetValue(targetProperty.Target, value, null);
      }
    }

    public static TSelf GatherFrom<TSelf, TSource>(this TSelf self, TSource source)
    {
      PropertyInfo[] sourceAllProperties = source.GetType().GetProperties();
      foreach (PropertyInfo sourceProperty in sourceAllProperties)
      {
        PropertyInfo selfProperty = self.GetType().GetProperty(sourceProperty.Name);
        if (selfProperty != null && selfProperty.CanWrite)
        {
          try
          {
            var sourceValue = sourceProperty.GetValue(source, null);
            selfProperty.SetValue(self, sourceValue, null);
          }
          catch (Exception)
          {
            //TODO: Add this again when log is implemented - Debug.WriteLine("Couldn't parse property: {0}", sourceProperty.Name);
          }
        }
      }

      return self;
    }

    /// <summary>
    ///   Skips complex properties
    /// </summary>
    public static bool IsModified<TSelf, TSource>(this TSelf self, TSource source)
    {
      PropertyInfo[] sourceAllProperties = source.GetType().GetProperties();
      foreach (PropertyInfo sourceProperty in sourceAllProperties)
      {
        PropertyInfo selfProperty = self.GetType().GetProperty(sourceProperty.Name);
        if (selfProperty == null)
        {
          continue;
        }

        if (selfProperty.CanRead && (selfProperty.PropertyType.IsPrimitive || selfProperty == typeof(string)))
        {
          var sourceValue = sourceProperty.GetValue(source, null);
          var selfValue = selfProperty.GetValue(self, null);
          if (!Convert.ChangeType(selfValue, selfProperty.PropertyType).Equals(Convert.ChangeType(sourceValue, selfProperty.PropertyType)))
          {
            return true;
          }
        }
      }

      return false;
    }

    private static TargetProperty GetTargetProperty(object source, string propertyName)
    {
      if (!propertyName.Contains("."))
      {
        return new TargetProperty { Target = source, Property = source.GetType().GetProperty(propertyName) };
      }

      string[] propertyPath = propertyName.Split('.');
      var targetProperty = new TargetProperty { Target = source, Property = source.GetType().GetProperty(propertyPath[0]) };
      for (int propertyIndex = 1; propertyIndex < propertyPath.Length; propertyIndex++)
      {
        propertyName = propertyPath[propertyIndex];
        if (!string.IsNullOrEmpty(propertyName))
        {
          targetProperty.Target = targetProperty.Property.GetValue(targetProperty.Target, null);
          targetProperty.Property = targetProperty.Target.GetType().GetProperty(propertyName);
        }
      }

      return targetProperty;
    }

    private class TargetProperty
    {
      #region Properties
      public bool IsValid
      {
        get
        {
          return Target != null && Property != null;
        }
      }
      public PropertyInfo Property { get; set; }
      public object Target { get; set; }
      #endregion
    }
    #endregion
  }
}
